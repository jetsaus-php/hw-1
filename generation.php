<?php
    $title = 'Генерация кода';
    $h4 = 'Генерация кода из переменных PHP:';
    $year = date('Y');
?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?= $title; ?></title>
</head>
<body>
    <h4><?= $h4; ?></h4>
    <span><?= "Сейчас $year год!"; ?></span>
</body>
</html>