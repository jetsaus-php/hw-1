<?php
/*
 * Обмен значениями 2-х переменных без посредника
 */
    $title = 'Обмен значениями';
    $h4 = 'Обмен значениями 2-х переменных без посредника:';
    $a = 1;
    $b = 2;
?>

<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?= $title; ?></title>
</head>
<body>
<?php
    echo "<h4>$h4</h4>";
    echo "Даны переменные: \$a = $a, \$b = $b <br><br>";
    echo 'Поменяли их значения местами: ';
        // Смена значений местами
        $a = $a + $b;
        $b = $a - $b;
        $a = $a -$b;
    echo "\$a = $a, \$b = $b <br>";
?>
</body>
</html>