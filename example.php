<?php
/*
 * Примеры из методички
 */
echo '<h3>Примеры из методички:</h3>';

echo '<h4>Вывод строк:</h4>';
$name = "GeekBrains user";
echo "Hello, $name!<br>";

echo '<h4>Константы:</h4>';
define ( 'MY_CONST' , 100 );
echo MY_CONST . '<br>';

echo '<h4>Типы данных:</h4>';
$int10 = 42;
$int2 = 0b101010;
$int8 = 052;
$int16 = 0x2A;
echo "Десятеричная система $int10 <br>";
echo "Двоичная система $int2 <br>";
echo "Восьмеричная система $int8 <br>";
echo "Шестнадцатеричная система $int16 <br>";

echo '<h4>Числа с плавающей точкой:</h4>';
$precise1 = 1.5;
$precise2 = 1.5e4;
$precise3 = 6E-8;
echo "$precise1 | $precise2 | $precise3";

echo '<h4>Строки:</h4>';
$a = 1;
echo "a <br>";
echo 'a';

echo '<h4>Конкатенация строк:</h4>';
$a = 'Hello,';
$b = 'world';
$c = $a . $b;
echo $c . '<br>';

echo '<h4>Приведение типов:</h4>';
$a = 10;
$a = (boolean)$a;
var_dump($a);

echo '<h4>Математические операции:</h4>';
$a = 4;
$b = 5;
echo "\$a = $a, \$b = $b <br><br>";
echo '$a + $b = ' . $a + $b . '<br>';       // сложение
echo '$a * $b = ' . $a * $b . '<br>';       // умножение
echo '$a - $b = ' . $a - $b . '<br>';       // вычитание
echo '$a / $b = ' . $a / $b . '<br>';       // деление
echo '$a % $b = ' . $a % $b . '<br>';       // остаток от деления
echo '$a ** $b = ' . $a ** $b . '<br>';     // возведение в степень
echo '$a += $b = ' . ($a += $b) . '<br>';   // операция с присвоением значения

echo '<h4>Инкремент/декремент:</h4>';
$a = 0;
echo "\$a = $a <br><br>";
echo '$a++ = ' . $a++ . '<br>';// Постинкремент
echo '++$a = ' . ++$a . '<br>'; // Преинкремент
echo '$a-- = ' . $a-- . '<br>'; // Постдекремент
echo '--$a = ' . --$a . '<br>'; // Предекремент

echo '<h4>Операции сравнения:</h4>';
$a = 4;
$b = 5;
echo "\$a = $a, \$b = $b <br><br>";
echo '($a == $b) => '; var_dump($a == $b); echo '<br>';      // Сравнение по значению
echo '($a === $b) => '; var_dump ($a === $b); echo '<br>';   // Сравнение по значению и типу
echo '($a > $b) => '; var_dump ($a > $b);  echo '<br>';         // Больше
echo '($a < $b) => '; var_dump ($a < $b);  echo '<br>';         // Меньше
echo '($a <> $b) => '; var_dump ($a <> $b);  echo '<br>';       // Не равно
echo '($a != $b) => '; var_dump ($a != $b);  echo '<br>';       // Не равно
echo '($a !== $b) => '; var_dump ($a !== $b);  echo '<br>';     // Не равно без приведения типов
echo '($a <= $b) => '; var_dump ($a <= $b);  echo '<br>';       // Меньше или равно
echo '($a >= $b) => '; var_dump ($a >= $b);  echo '<br>';       // Больше или равно
